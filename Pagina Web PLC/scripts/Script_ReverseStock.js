const initApp = () => {
  //console.log('initApp');
  /**
   * Reload automatico dei dati
   */
  window.addEventListener("load", () => {
    setInterval(function () {
      fetch('Variabili_Lettura.htm')
        .then(response => response.json())
        .then(json => {
          document.querySelectorAll('.js-show-data').forEach(function (el) {
            el.innerHTML = json[el.id]
          })
        });
    }, 100);
  })

  /**
   * prendo tutti gli input
   */
  document.querySelectorAll('.js-input').forEach(function (el) {
    if (el.classList.contains('js-input-press')) {
      console.log('input')
      el.addEventListener("mousedown", function (e) {
        e.preventDefault();
        console.log('press')
        postvar(el, true)
      })
      el.addEventListener("mouseup", function (e) {
        e.preventDefault();
        console.log('release')
        postvar(el, false)
      })
    } else if (el.classList.contains('js-input-click')) {
      el.addEventListener("click", function (e) {
        e.preventDefault();
        postvar(el, true)
        setTimeout(function () {
          postvar(el, false)
        }, 10)
      })
    } else {
      el.addEventListener("click", function (e) {
        e.preventDefault();
        postvar(el)
      })
    }
  });

  function postvar(el, value = null) {
    if (value == null) {
      value = el.value
    }
    console.log(value, el.dataset.input)
    var httpRequest = new XMLHttpRequest()
    httpRequest.open('POST', 'Variabili_Scrittura.htm')
    httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    httpRequest.send(el.dataset.input + '=' + encodeURIComponent(value))
  }
} // initApp


/** Ready
 ---------------------------------------------------------*/

/**
 * Ready with support for IE
 * https://stackoverflow.com/q/63928043/1252920
 */
if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
  initApp();
} else {
  document.addEventListener('DOMContentLoaded', initApp);
}
